using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;

namespace Reveil.Backend.Notifications {
  public class DevNullSender : INotificationSender {
    public string Notifier => "/dev/null";
    public Task<Result<Notification, NotificationError>> SendAsync(Notification notification) {
      return Task.FromResult(Result.Success<Notification, NotificationError>(notification));
    }
  }
}
