using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.BusinessLogic {
  public class UserBl : IUsersBl {
    private readonly IAuthenticationProviderFactory _authenticationProviderFactory;

    public UserBl(IAuthenticationProviderFactory authenticationProviderFactory) {
      _authenticationProviderFactory = authenticationProviderFactory;
    }

    public async Task<Result<UserResponse, ErrorResponse>> GetUser(string userId) {
      var authProvider = _authenticationProviderFactory.Create();
      var userResult = await authProvider.GetUser(userId);

      if (userResult.IsFailure)
        return Result.Failure<UserResponse, ErrorResponse>(ErrorResponse.Create(HttpStatusCode.InternalServerError, userResult.Error));

      if (userResult.IsSuccess && userResult.Value.HasNoValue)
        return Result.Failure<UserResponse, ErrorResponse>(ErrorResponse.Create(HttpStatusCode.NotFound, $"User with Id {userId} not found"));

      var userResponse = UserResponse.Create(userResult.Value.Value, HttpStatusCode.OK, $"Found User with Id {userId}");
      return Result.Success<UserResponse, ErrorResponse>(userResponse);
    }
  }
}
