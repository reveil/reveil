using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Authentication {
  public class AuthenticationChainOfResponsibility : IAuthenticationProvider {
    private readonly IEnumerable<IAuthenticationProvider> _providers;

    public AuthenticationChainOfResponsibility(IEnumerable<IAuthenticationProvider> providers) {
      _providers = providers;
    }

    public async Task<Result<Maybe<UserEntity>>> GetUser(string userId) {
      var userResults = await Task.WhenAll(_providers.Select(s => s.GetUser(userId)));
      return userResults.Any(a => a.IsFailure)
        ? Result.Failure<Maybe<UserEntity>>(userResults.First(a => a.IsFailure).Error)
        : userResults.First(u => u.IsSuccess && u.Value.HasValue);
    }

    public async Task<Result<bool>> AuthenticateAsync(UserEntry entry) {
      var authResults = await Task.WhenAll(_providers.Select(s => s.AuthenticateAsync(entry)));

      if (authResults.Any(a => a.IsFailure)) {
        return Result.Failure<bool>(authResults.First(a => a.IsFailure).Error);
      }

      return authResults.Any(a => a.IsSuccess && a.Value);
    }
  }
}
