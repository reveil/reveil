using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Notifications;

namespace Reveil.Backend.Interfaces {
  public interface INotificationSender {
    string Notifier { get;  }
    Task<Result<Notification, NotificationError>> SendAsync(Notification notification);
  }
}
