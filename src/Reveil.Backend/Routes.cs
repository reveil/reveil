namespace Reveil.Backend {
  public static class Routes {
    public const string GetMessageRoute = "/cases/{caseId}/messages/{messageId}";
    public const string GetJournalRoute = "/cases/{caseId}/journal/{journalId}";
  }
}
