using Microsoft.Extensions.Options;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public class GenericFileSystemRepository<T> : AbstractGenericFileSystemPersistedRepository<T> where T : BaseEntity {
    public GenericFileSystemRepository(IOptions<PersistenceConfiguration> persistenceConfiguration)
      : base(persistenceConfiguration) {
    }
  }
}
