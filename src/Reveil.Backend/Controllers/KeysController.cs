using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class KeysController {
    [HttpGet("/cases/{caseId}/keys/private", Name = nameof(GetPrivatKeyForCase))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(KeyResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetPrivatKeyForCase() {
      return new KeyResponse {
        Key = Guid.NewGuid().ToString(),
        Message = "That's the private key that you'll need to decrypt messages. Not",
        DateTime = DateTime.UtcNow,
        IsSuccess = true,
        StatusCode = HttpStatusCode.OK
      };
    }
  }
}
