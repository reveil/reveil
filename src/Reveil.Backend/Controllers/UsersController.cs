using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class UsersController : ControllerBase {
    private readonly IUsersBl _businessLogic;

    public UsersController(IUsersBl businessLogic) {
      _businessLogic = businessLogic;
    }

    [HttpGet("/users/{userId}", Name = nameof(GetUser))]
    [ProducesResponseType(typeof(UserResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetUser(string userId) =>
      _businessLogic
        .GetUser(userId)
        .GetAwaiter()
        .GetResult()
        .MatchAndSetStatusCode(HttpContext);
  }
}
