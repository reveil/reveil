// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;

namespace Reveil.Backend.Models.Api.Responses.Primitives {
  public class FrontendI18NConfiguration {
    public string DefaultLanguageCode { get; set; }
    public string DefaultCountryCode { get; set; }
    public TimeZoneInfo TimeZoneInfo { get; set; }
  }
}
