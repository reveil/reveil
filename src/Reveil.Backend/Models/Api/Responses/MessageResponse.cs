// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class MessageResponse : SuccessResponse {
    public string Id { get; set; }
    public string CaseId { get; set; }
    public string Data { get; set; }
    public string AuthorId { get; set; }
    public DateTime CreationDate { get; set; }
    public int DataProcessingMethodHint { get; set; }

    public static MessageResponse Create(MessageEntity entity, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new MessageResponse {
        Id = entity.Id,
        CaseId = entity.CaseId,
        AuthorId = entity.AuthorId,
        CreationDate = entity.CreationDate,
        DataProcessingMethodHint = entity.DataProcessingMethodHint,
        Data = entity.Data,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
