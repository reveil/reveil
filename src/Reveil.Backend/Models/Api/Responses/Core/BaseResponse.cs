// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;

namespace Reveil.Backend.Models.Api.Responses.Core {
  public class BaseResponse {
    public bool IsSuccess { get; set; }
    public DateTime DateTime { get; set; }
    public HttpStatusCode StatusCode { get; set; }
  }
}
