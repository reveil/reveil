// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using System.Collections.Generic;

namespace Reveil.Backend.Models.Configuration.Notification {
  public class SmtpNotificationTargetConfiguration : NotificationTargetConfigurationBase {
    public string SenderName { get; set; } = "Reveil Mailer";
    public string SenderEmail { get; set; }
    public List<SmtpRecipientConfiguration> Recipients { get; set; } = new();
    
    public string SmtpServer { get; set; }
    public int SmtpPort { get; set; }
    public bool UseSsl { get; set; }

    public bool UseSmtpAuthentication { get; set; } = true;
    public string SmtpServerUsername { get; set; }
    public string SmtpServerPassword { get; set; }
  }
}
