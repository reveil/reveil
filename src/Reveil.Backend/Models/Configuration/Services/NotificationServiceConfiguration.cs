using System;

namespace Reveil.Backend.Models.Configuration.Services {
  public class NotificationServiceConfiguration {
    public bool EnableNotifications { get; set; }
    public TimeSpan SendInterval { get; set; }
  }
}
