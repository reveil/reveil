using System;
using Reveil.Backend.Structures;

namespace Reveil.Backend.Models.Entities {
  public class JournalEntity : BaseEntity {
    public string CaseId { get; set; }
    public DateTime CreationDate { get; set; }
    public JournalEntryType JournalType { get; set; }
  }
}
