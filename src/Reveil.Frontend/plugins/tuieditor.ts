import Vue from 'vue'
import { Viewer, Editor } from '@toast-ui/vue-editor'

Vue.component(
    'TuiViewer', 
    { 
        extends:  Viewer,
    }
);

Vue.component(
    'TuiEditor', 
    { 
        extends: Editor,
    }
);