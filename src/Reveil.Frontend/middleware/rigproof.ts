import { Middleware } from '@nuxt/types'

const rigproof: Middleware = async (context) => {
    var redirect_to = '/e/rigproof';
    if(document.location.protocol !== 'https:' && context.route.path !== redirect_to) {
        context.redirect(redirect_to);
    }
}

export default rigproof
