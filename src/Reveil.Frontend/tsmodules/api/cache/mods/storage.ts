import { ApiCacheModsIMod } from './imod'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiCacheModsStorage<T> implements ApiCacheModsIMod<T> {
    private name: string
    private inject: ApiEndpointExtensionLayer

    constructor(name: string, inject: ApiEndpointExtensionLayer) {
        this.name = name
        this.inject = inject
    }

    GetCache(): T | null {
        return JSON.parse(localStorage.getItem(this.name) || '{}') as T || null
    }

    SetCache(data: T): void {
        localStorage.setItem(this.name, JSON.stringify(data))
    }

    ClearCache() {
        localStorage.removeItem(this.name)
    }

    WipeCache() {
        localStorage.clear()
    }
}
