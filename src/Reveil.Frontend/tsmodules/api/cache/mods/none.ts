import { ApiCacheModsIMod } from './imod'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiCacheModsNone<T> implements ApiCacheModsIMod<T> {
    private name: string
    private inject: ApiEndpointExtensionLayer

    constructor(name: string, inject: ApiEndpointExtensionLayer) {
        this.name = name
        this.inject = inject
    }

    GetCache(): T | null {
        return null
    }

    SetCache(data: T): void {
        
    }

    ClearCache() {
        
    }

    WipeCache() {
        
    }
}
