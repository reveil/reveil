import { ApiHashIMod } from './imod'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

import pbkdf2 from 'pbkdf2'


export class ApiHash01 implements ApiHashIMod {
    public static Hash(data: string, inject: ApiEndpointExtensionLayer): Promise<string> {
        return Promise.resolve(pbkdf2.pbkdf2Sync(data, "reveil", 999, 64, "sha256").toString('hex'))
    }
}
