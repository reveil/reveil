import { ApiCryptModsIMod } from './imod'

import { Base64 } from '~/tsmodules/base64'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiCryptModsPlain<T> implements ApiCryptModsIMod<T> {
    constructor(inject: ApiEndpointExtensionLayer) {

    }

    Encrypt(data: T): string {
        return new Base64().StrToB64(JSON.stringify(data))
    }

    Decrypt(data: string): T {
        return JSON.parse(new Base64().B64ToStr(data)) as T
    }
}
