export interface IModelMessagePayload {
    subject: string,
    message: string,
}
