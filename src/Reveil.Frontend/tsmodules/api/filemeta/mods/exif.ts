import { ApiFileMetaIMod } from './imod'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

import { remove } from './piexifjs'

export class ApiFileMetaModsExif implements ApiFileMetaIMod {
    public static WipeMeta(file: File, content: string, inject: ApiEndpointExtensionLayer): string {
        try {
            return remove(content)
        }
        catch(e) {
            return content
        }
    }
}
