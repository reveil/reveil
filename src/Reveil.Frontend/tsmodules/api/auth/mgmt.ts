import * as api from '~/api'

import { IModelAuthMgmt } from './models'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiAuthMgmt extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< auth
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async AuthMgmtLogin(username: string, password: string, remember: boolean): Promise<IModelAuthMgmt | null> {
        var cache = ApiCache.Proxy<IModelAuthMgmt>(this.eel!.CacheType!, 'authmgmt', this.eel!)
        if(cache.GetCache() !== null && cache.GetCache()?.response.validityDuration?.seconds! >= 30) {
            return Promise.resolve(cache.GetCache())
        }

        return new api.AuthenticationApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).authenticate({ username, password } as api.AuthenticationRequest)
        .then((e) => {
            if(e.data.isSuccess === true) {
                var model = { 
                    username: username, 
                    password: password, 
                    remember: remember, 
                    authtype: "mgmt",
                    response: e.data,
                } as IModelAuthMgmt
    
                cache.SetCache(model)
                return model;
            }
            this.AuthMgmtLogout()
            return null
        })
        .catch((e) => {
            this.AuthMgmtLogout()
            return null
        })
    }

    AuthMgmtState(): IModelAuthMgmt | null {
        return ApiCache.Proxy<IModelAuthMgmt>(this.eel!.CacheType!, 'authmgmt', this.eel!).GetCache() || null
    }

    async AuthMgmtLogout(): Promise<boolean> {
        ApiCache.Proxy<IModelAuthMgmt>(this.eel!.CacheType!, 'authmgmt', this.eel!).ClearCache()
        return true;
    }
}
