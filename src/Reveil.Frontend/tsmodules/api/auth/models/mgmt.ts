import * as api from '~/api'
import { IModelAuthBase } from './base'

export interface IModelAuthMgmt extends IModelAuthBase {
    response: api.AuthenticationResponse,
}
