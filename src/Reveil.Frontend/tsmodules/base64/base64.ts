export class Base64 {
    StrToB64(s: string): string {
        return Buffer.from(s).toString('base64')
    }
      
    B64ToStr(s: string): string {
        return Buffer.from(s, 'base64').toString('binary')
    }
}