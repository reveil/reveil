# reveil

<img src="/docs/assets/puzzle.png" alt="Reveil" width="128"/>

![Pipeline](https://gitlab.com/reveil/reveil/badges/main/pipeline.svg?ignore_skipped=true)
![Release](https://gitlab.com/reveil/reveil/-/badges/release.svg)
[![Quality Gate Status](https://sonarqube.reveil.rdev.at/api/project_badges/measure?project=reveil_reveil_AYPdr3TinoRD2MJpi4N_&metric=alert_status&token=63e549cc0526da07b0fa52777085e91fd674176a)](https://sonarqube.reveil.rdev.at/dashboard?id=reveil_reveil_AYPdr3TinoRD2MJpi4N_)
[![Security Rating](https://sonarqube.reveil.rdev.at/api/project_badges/measure?project=reveil_reveil_AYPdr3TinoRD2MJpi4N_&metric=security_rating&token=63e549cc0526da07b0fa52777085e91fd674176a)](https://sonarqube.reveil.rdev.at/dashboard?id=reveil_reveil_AYPdr3TinoRD2MJpi4N_)
[![Maintainability Rating](https://sonarqube.reveil.rdev.at/api/project_badges/measure?project=reveil_reveil_AYPdr3TinoRD2MJpi4N_&metric=sqale_rating&token=63e549cc0526da07b0fa52777085e91fd674176a)](https://sonarqube.reveil.rdev.at/dashboard?id=reveil_reveil_AYPdr3TinoRD2MJpi4N_)
[![Reliability Rating](https://sonarqube.reveil.rdev.at/api/project_badges/measure?project=reveil_reveil_AYPdr3TinoRD2MJpi4N_&metric=reliability_rating&token=63e549cc0526da07b0fa52777085e91fd674176a)](https://sonarqube.reveil.rdev.at/dashboard?id=reveil_reveil_AYPdr3TinoRD2MJpi4N_)
[![Vulnerabilities](https://sonarqube.reveil.rdev.at/api/project_badges/measure?project=reveil_reveil_AYPdr3TinoRD2MJpi4N_&metric=vulnerabilities&token=63e549cc0526da07b0fa52777085e91fd674176a)](https://sonarqube.reveil.rdev.at/dashboard?id=reveil_reveil_AYPdr3TinoRD2MJpi4N_)

Reveil is a F(L)OSS whistleblower platform compliant with the general whistleblower regulation governed by the european union. It's written in ASP.NET (.NET 6) for the Backend and Vue (Nuxt) for the Frontend. It's open and interconnectable with your existing infrastructure, and its primary focus is the anonymity of the whistleblower. It's free, self-hostable, and extensible and open to modification.

## Why Reveil?

Unlawful activities and abuse of law may occur in any organisation, whether private or public, big or small. They can take many forms, corruption, fraud, businesses’ malpractice or negligence. And if they are not addressed, it can result in serious harm to the public interest. People who work for an organisation or are in contact with it in their work-related activities are often the first to know about such occurrences and are, therefore, in a privileged position to inform those who can address the problem. Whistleblowers, i.e. persons who report (within the organisation concerned or to an outside authority) or disclose (to the public) information on a wrongdoing obtained in a work-related context, help preventing damage and detecting threat or harm to the public interest that may otherwise remain hidden.

However, at European and national level the protection of whistleblowers is uneven and fragmented. As a consequence whistleblowers are often discouraged from reporting their concerns for fear of retaliation. For these reasons, on 23 April 2018, the European Commission presented a package of initiatives including a Proposal for Directive on the protection of persons reporting on breaches of Union law and a Communication, establishing a comprehensive legal framework for whistleblower protection for safeguarding the public interest at European level, setting up easily accessible reporting channels, underlining the obligation to maintain confidentiality and the prohibition of retaliation against whistleblowers and establishing targeted measures of protection.

The Directive - (EU) 2019/1937 of the European Parliament and of the Council of 23 October 2019 on the protection of persons who report breaches of Union law - was adopted on 23 October 2019 and entered into force on 16 December 2019. Member States have until 17 December 2021 to transpose it into their national laws.

## Further reading

- [Development](docs/development.md)
- [Backend Documentation](docs/backend.md)

## Guide on deployment

A helpful guide on how to install reveil safe and secure. Also provides several configrations to build on.

- [Reveil-Deploy](https://gitlab.com/reveil/reveil-deploy)