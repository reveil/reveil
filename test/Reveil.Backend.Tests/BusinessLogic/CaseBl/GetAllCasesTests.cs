using System.Linq;
using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.BusinessLogic;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.CaseBl {
  public class GetAllCasesTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public GetAllCasesTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData(0, RepoType.InMemory)]
    [InlineData(1, RepoType.InMemory)]
    [InlineData(10, RepoType.InMemory)]
    [InlineData(99, RepoType.InMemory)]
    [InlineData(0, RepoType.FilePersisted)]
    [InlineData(1, RepoType.FilePersisted)]
    [InlineData(10, RepoType.FilePersisted)]
    [InlineData(99, RepoType.FilePersisted)]
    public void TestGetAllCasesReturns200OkResponse(int caseCount, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      Enumerable
        .Range(0, caseCount)
        .ToList()
        .ForEach(c => cases.Add(new CaseEntity {Id = c.ToString()}));

      var bl = new CasesBl(cases, journals, messages, _devNullSenderFactory);
      var r = bl.GetAllCases();
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(caseCount);
      r.Value.Count.Should().Be(caseCount);

      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
    }
  }
}
